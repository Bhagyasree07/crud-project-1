/*This java project dynamically handles DDL and DML operations
it allows you to dynamically create tables of arbitary size(variable, no of columns) and data type.
Based on datatype the appropriate table is created which is manipultion via CRUD operations*/

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static java.lang.Integer.parseInt;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

//Class for Connectong Servers
class DAO{
            
        //creates a connection to DataBase
        public static Connection getConnection(){
            Connection con = null;
            try {
                Class.forName("oracle.jdbc.driver.OracleDriver"); 
                con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl","scott","tiger");
            }
            catch (ClassNotFoundException | SQLException ex) {
                
                Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            return con;
       }
        
}


@WebServlet(urlPatterns = {"/Servlet_JDBC"})
public class MyServlet extends HttpServlet {
 public static Connection con;
    String s = null;
 
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.sql.SQLException
     */
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        Connection con1=DAO.getConnection();
        response.setContentType("text/html;charset=UTF-8");
        
        //Checks the apprpriate function to be executed and returns the string 
        try (PrintWriter out = response.getWriter()) {
            if(request.getParameter("CR")!=null){
                    s=DBops.createTable(request.getParameter("table_name"), request.getParameter("num_of_cols"), request);
            }
            if(request.getParameter("RT")!=null){
                    s=DBops.retrieveData(request.getParameter("table_name"), request.getParameter("pkey"), request.getParameter("pkey_value"));
            }
            
            if(request.getParameter("UP")!=null){
                    s=DBops.updateData(request.getParameter("table_name"), request.getParameter("num_of_cols"),
                                       request.getParameter("pkey"), request.getParameter("pkey_value"), request);
            }
            if(request.getParameter("DL")!=null){
                    s=DBops.deleteData(request.getParameter("table_name"), request.getParameter("pkey"), request.getParameter("pkey_value"));
            }
            
            //executes the statement and returns the data
            //out.println("<p>"+s+"</p>");
            Statement stmt=con1.createStatement();  
            ResultSet rs=stmt.executeQuery(s);
            out.println("<p>"+rs+"</p>");
        }
        
        //catches any exception if any gives the appropriate exception
        catch(Exception e){
            out.println("<p>ERROR OCCURED!<br>ERROR DESCRIPTION : "+e+"</p>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    
     try {
         processRequest(request, response);
     } catch (SQLException ex) {
         Logger.getLogger(MyServlet.class.getName()).log(Level.SEVERE, null, ex);
     }
     
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
     try {
         processRequest(request, response);
     } catch (SQLException ex) {
         Logger.getLogger(MyServlet.class.getName()).log(Level.SEVERE, null, ex);
     }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

class DBops {
    
    //Method creates table
    public static String createTable(String table_name, String num_of_cols, HttpServletRequest request){
        String s1="CREATE TABLE "+table_name+"{";
        int n=parseInt(num_of_cols);
        int i;
	for(i=0;i<n-1;i++){
            s1+=request.getParameter("Attribute"+i)+" "+request.getParameter("Att"+i)+", ";
	}
	s1+=request.getParameter("Attribute"+i)+" "+request.getParameter("Att"+i)+"}";
        return s1;
        }
        
    //Method retrieves data from DB
    public static String retrieveData(String table_name, String pkey, String pkey_value){
        return "SELECT * FROM "+table_name+" WHERE "+pkey+" = "+pkey_value+";";
    }
    
    //Method updates data in DB
    public static String updateData(String table_name, String num_of_cols, String pkey, String pkey_value, HttpServletRequest request) {
        int n=parseInt(num_of_cols);
        int i;
        String s2="UPDATE "+table_name+" ";
        for( i=0;i<n-1;i++){
            s2+=request.getParameter("Attribute"+i)+" = "+request.getParameter("Att"+i)+" ,";
        }
        s2+=request.getParameter("Attribute"+i)+" = "+request.getParameter("Att"+i)+"WHERE "+pkey+" = "+pkey_value+ ";" ;
        return s2;
            
    }
    
    //Method deletes data from DB
    public static String deleteData(String table_name, String pkey, String pkey_value) {
        return "DELETE FROM "+table_name+" WHERE "+pkey+" = "+pkey_value+";";
    }
    
}
